MoSARTOmegaServer 
================

Web Application for MoSART Omega built on Revel, Go, and Cassandra DB, controller for the Computational Engine (MATLAB), interfaces with 
AscAmadeusMobile by receiving inputs for a model and returning results to the requesting client.

Dependencies:

* Revel Web Framework for Go
	* http://robfig.github.io/revel/
* Go Programming Language
	* http://golang.org/
* gocql
	* https://github.com/tux21b/gocql
* Apache Cassandra database
	* http://cassandra.apache.org/
* Python 2.7.3
	* http://www.python.org/
* Go-Python
	* https://github.com/sbinet/go-python
* Python-Matlab-Bridge
	* https://github.com/jaderberg/python-matlab-bridge
* MATLAB 2012b for Linux
	* http://www.mathworks.com/support/sysreq/release2012b/linux.html
	
