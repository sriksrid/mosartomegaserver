#from  BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
try:
    from http.server import BaseHTTPRequestHandler, HTTPServer
except:
    from  BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer

import os
from os import *
try:
    from pymatbridge import Matlab
except:
    print("Can't load pymatbridge");

try:
    from urlparse import urlparse, parse_qs
except:
    from urllib.parse import urlparse, parse_qs

#Create custom HTTPRequestHandler class
class Handler(BaseHTTPRequestHandler):
    #handle GET command
    def do_GET(self):
        #Parse the web data
        dc_WebData = parse_qs(urlparse('http://google.com'+self.path).query);
        rootdir = os.path.dirname(os.path.abspath(__file__)) #file location
        
        #Check if the key for the module name exists
        
        #Now select the right Matlab function and pass the module
        if 'name' not in dc_WebData:
            #Data not useful - can't identify the function to call!
            print("1");
        else:
            s_Name = dc_WebData['name'][0];
            if(s_Name=='Fn1'):
                print("Function 1");
                d_Value = float(dc_WebData['Value'][0]);
                if(b_RunMatlab == 1):
                    res=mlab.run_func('/home/ssridha7/Matlab/test_fn.m',{'value': d_Value});
		    self.wfile.write('<html><head></head><body>Success');
		    self.wfile.write('<br />New line</body></html>');
		    #import pdb; pdb.set_trace();
                    #print(res);

        #try:
        #    if self.path.endswith('.htm'):
        #        f = open(os.path.abspath('.'+self.path),'r') #open requested file                
        #        #f = open(rootdir + self.path) #open requested file
                
        #        #send code 200 response
        #        self.send_response(200)

        #        #send header first
        #        self.send_header('Content-type','text-html')
        #        self.end_headers()

        #        #send file content to client
        #        self.wfile.write(f.read())
        #        f.close()
        #        return
            
        #except IOError:
        #    self.send_error(404, 'file not found')
    
def run():
    print('http server is starting...')

    #ip and port of servr
    #by default http server port is 80
    server_address = ('10.206.145.221', 8010)
    httpd = HTTPServer(server_address, Handler)
    print('http server is running...')
    httpd.serve_forever()

def mworker():
    global mlab
    mlab = Matlab(matlab='/usr/local/MATLAB/R2012b/bin/matlab');
    mlab.start();    

b_RunMatlab = 1;

try:
    mworker();
except:
    print("Can't start worker");
    b_RunMatlab = 0;

run()
