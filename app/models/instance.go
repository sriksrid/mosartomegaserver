package models

import (
		"github.com/robfig/revel"
)

// Contains info necessary to manage and launch a Matlab Instance
type Instance struct {
	Id		int
	UserId		int
	Filename	string
	Input1		string
	Input2		string
	Input3		string
	Input4		string

	//Transient
	Running		bool
}

func (instance Instance) Validate(v *revel.Validation) {
	v.Required(instance.Filename)
}
