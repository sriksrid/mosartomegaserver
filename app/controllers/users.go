package controllers

import (
	"bitbucket.org/sriksrid/mosartomegaserver/app/models"
	"bitbucket.org/sriksrid/mosartomegaserver/app/routes"
	"code.google.com/p/go.crypto/bcrypt"
	"github.com/robfig/revel"
	"os"
	"path"
	"strings"

)

type Users struct {
	Application
}

func (c Users) checkUser() revel.Result {
	if user := c.connected(); user == nil {
		c.Flash.Error("Please log in first")
		return c.Redirect(routes.Application.Index())
	}
	return nil
}

func (c Users) Index() revel.Result {
	results, err := c.Txn.Select(models.User{},
		`select * from User where UserId = ?`, c.connected().UserId)
	if err != nil {
		panic(err)
	}

	var users []*models.User
	for _, r := range results {
		u := r.(*models.User)
		users = append(users, u)
	}

	return c.Render(users)
}

func (c Users) List(search string, size, page int) revel.Result {
	if page == 0 {
		page = 1
	}
	nextPage := page + 1
	search = strings.TrimSpace(search)

	var users []*models.User
	if search == "" {
		users = loadUsers(c.Txn.Select(models.User{},
			`select * from User limit ?, ?`, (page-1)*size, size))
	} else {
		search = strings.ToLower(search)
		users = loadUsers(c.Txn.Select(models.User{},
			`select * from User where lower(Username) like ? or lower(Name) like ?
 limit ?, ?`, "%"+search+"%", "%"+search+"%", (page-1)*size, size))
	}

	return c.Render(users, search, size, page, nextPage)
}

func loadUsers(results []interface{}, err error) []*models.User {
	if err != nil {
		panic(err)
	}
	var users []*models.User
	for _, r := range results {
		users = append(users, r.(*models.User))
	}
	return users
}

func (c Users) loadUserById(id int) *models.User {
	h, err := c.Txn.Get(models.User{}, id)
	if err != nil {
		panic(err)
	}
	if h == nil {
		return nil
	}
	return h.(*models.User)
}

func (c Users) Show(id int) revel.Result {
	user := c.loadUserById(id)
	if user == nil {
		return c.NotFound("User %d does not exist", id)
	}
	title := user.Username
	return c.Render(title, user)
}

// Form for creating a new user
func (c Users) Create() revel.Result {
	return c.Render()
}

func (c Users) Save(user models.User, verifyPassword string) revel.Result {
	c.Validation.Required(verifyPassword)
	c.Validation.Required(verifyPassword == user.Password).
		Message("Password does not match")
	user.Validate(c.Validation)

	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.Application.SignUp())
	}

	user.HashedPassword, _ = bcrypt.GenerateFromPassword(
		[]byte(user.Password), bcrypt.DefaultCost)
	err := c.Txn.Insert(&user)
	if err != nil {
		panic(err)
	}

	// Create the user's matlab file directories
	userdir := path.Join(revel.BasePath, "matlab/users", user.Username)
	os.Mkdir(userdir, 0755)
	os.Mkdir(path.Join(userdir, "dat"), 0775)
	os.Mkdir(path.Join(userdir, "img"), 0775)
	os.Mkdir(path.Join(userdir, "src"), 0775)

	c.Flash.Success("User account " + user.Username + " created.")
	return c.Redirect(routes.Users.Index())
}

// Form for editing a user
func (c Users) Edit(id int) revel.Result {
	user := c.loadUserById(id)
	if user == nil {
		c.Flash.Error("User does not exist")
		return c.Redirect(routes.Users.Index())
	}

	return c.Render(user)
}

// Updates a User
func (c Users) Update(user models.User) revel.Result {
	c.Validation.Required(user.Username)
	c.Validation.Required(len(user.Username) > 2).
		Message("Username must be more than 2 characters")

	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.Users.Edit(user.UserId))
	}

	// Fetch the old user info
	oldUser := c.loadUserById(user.UserId)
	if oldUser == nil {
		c.Flash.Error("User does not exist")
		return c.Redirect(routes.Users.Index())
	}

	// Update the database with the new user info
	_, err := c.Txn.Update(&user)
	if err != nil {
		panic(err)
	}

	// Rename the user's matlab directory
	oldUserDir := path.Join(revel.BasePath, "matlab/users", oldUser.Username)
	userDir := path.Join(revel.BasePath, "matlab/users", user.Username)
	err = os.Rename(oldUserDir, userDir)
	if err != nil {
		panic(err)
	}


	if oldUser.Username != user.Username {
		c.Flash.Success("User account " + oldUser.Username + "  renamed to " + user.Username + ".")
	} else {
		c.Flash.Success("User account " + user.Username + " updated.")
	}
	return c.Redirect(routes.Users.Index())

}

// Deletes a User
func (c Users) Delete(id int) revel.Result {
	// Delete the user's matlab files and directories
	user := c.loadUserById(id)
	if user == nil {
		c.Flash.Error("User does not exist")
		return c.Redirect(routes.Users.Index())
	}
	userdir := path.Join(revel.BasePath, "matlab/users", user.Username)
	os.RemoveAll(userdir)

	// Delete the user from the database
	_, err := c.Txn.Delete(&models.User{UserId: id})
	if err != nil {
		panic(err)
	}
	c.Flash.Success("User deleted.")
	return c.Redirect(routes.Users.Index())
}
