package controllers

import (
	"bitbucket.org/sriksrid/mosartomegaserver/app/models"
	"bitbucket.org/sriksrid/mosartomegaserver/app/routes"
	"fmt"
	"github.com/robfig/revel"
	"path"
	"os"
	"os/exec"
)

type Matlab struct {
	Application
}

func (c Matlab) checkUser() revel.Result {
	if user := c.connected(); user == nil {
		c.Flash.Error("Please log in first")
		return c.Redirect(routes.Application.Index())
	}
	return nil
}


func (c Matlab) getInstance(id int) *models.Instance {
	i, err := c.Txn.Get(models.Instance{}, id)
	if err != nil {
		panic(err)
	}
	if i == nil {
		return nil
	}
	return i.(*models.Instance)
}

func (c Matlab) Index() revel.Result {
	results, err := c.Txn.Select(models.Instance{},
		`select * from Instance where UserId = ?`, c.connected().UserId)
	if err != nil {
		panic(err)
	}

	var instances []*models.Instance
	for _, r := range results {
		i := r.(*models.Instance)
		instances = append(instances, i)
	}

	return c.Render(instances)
}

// Form for creating an instance
func (c Matlab) Create() revel.Result {
	return c.Render()
}

// Saves a Matlab Instance
func (c Matlab) Save(i models.Instance) revel.Result {
	c.Validation.Required(i.Filename)
	c.Validation.Required(len(i.Filename) > 2).
		Message("Filename must be more than 2 characters")

	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.Matlab.Create())
	}
	i.UserId = c.connected().UserId


	err := c.Txn.Insert(&i)
	if err != nil {
		panic(err)
	}

	c.Flash.Success(i.Filename + " created.")
	return c.Redirect(routes.Matlab.Index())
}

// Form for editing an instance
func (c Matlab) Edit(id int) revel.Result {
	i := c.getInstance(id)
	if i == nil {
		c.Flash.Error("File does not exist")
		return c.Redirect(routes.Matlab.Index())
	}

	return c.Render(i)
}

// Updates a Matlab Instance
func (c Matlab) Update(i models.Instance) revel.Result {
	c.Validation.Required(i.Filename)
	c.Validation.Required(len(i.Filename) > 2).
		Message("Filename must be more than 2 characters")

	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.Matlab.Edit(i.Id))
	}
	i.UserId = c.connected().UserId


	_, err := c.Txn.Update(&i)
	if err != nil {
		panic(err)
	}

	c.Flash.Success(i.Filename + " updated.")
	return c.Redirect(routes.Matlab.Index())

}

// Execute the command to run MATLAB
func (c Matlab) Run(id int) revel.Result {
	i := c.getInstance(id)
	if i == nil {
		c.Flash.Error("File does not exist")
		return c.Redirect(routes.Matlab.Index())
	}
	dir := path.Join(revel.BasePath, "matlab")
	fmt.Println(dir)
	BashLog := fmt.Sprintf("%v/logs/bashlog_%v.txt", dir, i.Id)
	MatlabLog := fmt.Sprintf("%v/logs/matlablog_%v.txt", dir, i.Id)
	Run := fmt.Sprintf("-r \"cd %v,%v(%v,%v,%v),exit\"", dir, i.Filename, i.Input1, i.Input2, i.Input3)
	fmt.Println(Run)
	LogFile := fmt.Sprintf("-logfile %v > %v", MatlabLog, BashLog)
	InitLog(BashLog)
	InitLog(MatlabLog)
	cmd := exec.Command("/home/pgaines/MatlabNew/bin/matlab", "-nodesktop", "-nosplash", "-nodisplay", Run, LogFile)
	out, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println("Start process failed:" + err.Error())
		return nil
	}
	c.Flash.Success(i.Filename + " has executed with output: " + string(out))
	fmt.Print(string(out))
	fmt.Println(fmt.Sprintf("Matlab Instance %v has executed successfully", i.Id))
	i.Running = true

	return c.Redirect(routes.Matlab.Index())
}

// Deletes a Matlab Instance
func (c Matlab) Delete(id int) revel.Result {
	_, err := c.Txn.Delete(&models.Instance{Id: id})
	if err != nil {
		panic(err)
	}
	c.Flash.Success("Matlab instance deleted.")
	return c.Redirect(routes.Matlab.Index())
}

// Checks if a file or path exists, returns true if so, false otherwise
func PathExists(path string) bool {
	if _, err := os.Stat(path); err == nil {
		return false
	}
	return true
}
// Creates a file specified by path if it doesn't already exist
func InitLog(path string) {
	if !PathExists(path) {
		_, err := os.Create(path)
		if err != nil {
			fmt.Println(err)
		}
	}
}
