package tests

import (
	"bitbucket.org/tebeka/selenium"
	"github.com/robfig/revel"
	"time"
)

var code string = `
package main
import "fmt"

func main () {
	fmt.Println("Hello WebDriver!\n")
}
`

type AppTest struct {
	revel.TestSuite
}

func (t AppTest) Before() {
	println("Set up")
}

func (t AppTest) TestThatIndexPageWorks() {
	t.Get("/")
	t.AssertOk()
	t.AssertContentType("text/html")
}

func (t AppTest) TestThatLoginWorks() {
	// FireFox driver without specific version
	//caps := selenium.Capabilities{"browserName": "firefox"}
	//wd, _ := selenium.NewRemote(caps, "http://127.0.0.1:4444/wd/hub")

	caps := selenium.Capabilities{"browserName": "chrome"}
	wd, _ := selenium.NewRemote(caps, "http://127.0.0.1:9515")

	defer wd.Quit()
	// Get Revel web application
	//wd.Get("http://play.golang.org/?simple=1")
	wd.Get("http://mosartomega.org:9000/")

	// Enter username
	username, _ := wd.FindElement(selenium.ByCSSSelector, "#username")
	username.Clear()
	username.SendKeys("demo")

	// Enter password
	password, _ := wd.FindElement(selenium.ByCSSSelector, "#password")
	password.Clear()
	password.SendKeys("demo")

	btn, _ := wd.FindElement(selenium.ByCSSSelector, "#login")
	btn.Click()

	time.Sleep(time.Second * 2)

	// Get the result
	span, _ := wd.FindElement(selenium.ByCSSSelector, "#username")
	output, _ := span.Text()

	// Check that #username element contains "demo"
	t.Assert(output == "demo")
}

func (t AppTest) After() {
	println("Tear down")
}
