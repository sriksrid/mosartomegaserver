function dV_Vec = test_fn(d_Value)
%close all
d_Value=d_Value.value;
figure(10); clf;
dV_Time = 1:.01:10;
plot(dV_Time,dV_Time*d_Value);
grid on
xlabel('Time');
ylabel('Value');
dV_Vec = dV_Time*d_Value;
print(10,'-djpeg','/home/pgaines/gocode/src/bitbucket.org/sriksrid/mosartomegaserver/matlab/users/ssridha7/img/Fig1.jpg');
end
