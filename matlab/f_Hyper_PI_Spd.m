% This function designs a controller for velocity control of a hypersonic
% model using a PI controller

function s_RetStr = f_Hyper_PI_Spd(n_ModelChoice,d_OS,d_SettlingTime)
s_RetStr = '';

%% Load the correct model
switch(n_ModelChoice)
    case 1 % Rigid
        load ./DataFiles/RigidNom;
    case 2 % Flexible
        load ./DataFiles/FlexNom;
end

%% Extract the plant
ss_Plant = HSV_Trim_Data{1,1}.FER_Scaled; %#ok<USENS>
dM_A = ss_Plant.a;
dM_B = ss_Plant.b;
dM_C = ss_Plant.c(1,:);
dM_D = [0 0];

%% Augment with integrator
dM_A = blkdiag(dM_A,0);
dM_A(end,1:size(dM_A,1)-1) = dM_C;
dM_B = [dM_B;zeros(1,size(dM_B,2))];
dM_C = [0*dM_C 1];

%% Design the controller - just the PI part

% Assuming the FER-Velocity is first order (and stable), we can
% compute the desired CLTF in terms of zeta, wn
d_Zeta = abs(log(d_OS))/sqrt((log(d_OS)^2)+pi^2);
d_wn = 5/d_Zeta/d_SettlingTime;

% We want the CLTF to look like w^2/(s^2 + 2*zeta*wn*s + w^2)
% K = g*(s+z)/s,  P = b/(s+a) => CLTF: g*(s+z)*b/(s^2 + (a+gb)s + gbz)
% Prefilter = z/(s+z), g*z*b = w^2, (a+gb) = 2*zeta*w

d_a = dM_A(1,1);
d_b = dM_B(1,1);
d_g = (2*d_Zeta*d_wn-d_a)/d_b;
d_z = d_wn^2/d_g/d_b;
tf_K = tf([d_g d_g*d_z],[1 0]);

%% State feedback
dM_AOrig = dM_A; dM_BOrig = dM_B;

% Now fill in the State feedback portion using just elevator control and
% Non-Velocity states
dM_Kphi = [-d_g;zeros(size(dM_A,1)-2,1);-d_g*d_z]';
dM_A = dM_A + dM_B(:,1)*dM_Kphi;
dM_A = dM_A(2:end-1,2:end-1); dM_B = dM_B(2:end-1,:);
% Change dM_B to only have the elevator
dM_B = dM_B(:,2);
dM_Cz = ss_Plant.a(1,2:4);
dM_Dzu = 0;

% Now generate the stabilizing state feedback LMI
setlmis([]); n_LMICnt = 0;

% lmiM_Q = lmivar(1,[1 1;size(dM_A,1)-2 1;1 1]);
% [lmiM_YF,~,lmin_YF] = lmivar(2,[1 size(dM_A,1)-2]);
% lmiM_Y = lmivar(3,[0 lmin_YF 0]);

lmiM_Q = lmivar(1,[size(dM_A,1) 1]);
lmiM_Y = lmivar(2,[1 size(dM_A,1)]);

lmid_Cost = lmivar(1,[1 1]);

% Positive (semi)definite cost and Q matrix
n_LMICnt = n_LMICnt  + 1;
lmiterm([-n_LMICnt 1 1 lmiM_Q],1,1);
lmiterm([-n_LMICnt 2 2 lmid_Cost],1,1);

% Stability
n_LMICnt = n_LMICnt  + 1;
lmiterm([n_LMICnt 1 1 lmiM_Q],dM_A,1,'s');
lmiterm([n_LMICnt 1 1 lmiM_Y],dM_B,1,'s');

% % L2
n_LMICnt = n_LMICnt  + 1;
lmiterm([n_LMICnt 1 1 lmiM_Q],dM_A,1,'s');
lmiterm([n_LMICnt 1 1 lmiM_Y],dM_B,1,'s');
lmiterm([n_LMICnt 1 1 0],dM_B*dM_B');

lmiterm([n_LMICnt 2 1 lmiM_Q],dM_Cz,1);
lmiterm([n_LMICnt 2 1 lmiM_Y],dM_Dzu,1);

lmiterm([n_LMICnt 2 2 lmid_Cost],-1,1);

%% Solve the system
lmi_Prob = getlmis;
dV_Opts = zeros(5,1); dV_Opts(2) = 1e3; dV_Opts(1) = 1e-3;
n_NrLMIVars = decnbr(lmi_Prob);
dV_CostFnLMI = zeros(n_NrLMIVars,1); dV_CostFnLMI(decinfo(lmi_Prob,lmid_Cost))=1;

% Minimize the cost
[d_Cost,lmi_Soln] = mincx(lmi_Prob,dV_CostFnLMI,dV_Opts);

if(~isempty(d_Cost)) % A solution exists
    dM_Q = dec2mat(lmi_Prob,lmi_Soln,lmiM_Q);
    dM_Y = dec2mat(lmi_Prob,lmi_Soln,lmiM_Y);
    dM_K = dM_Y/dM_Q;
    d_Cost = sqrt(d_Cost); % The L-2 gain
    display([' === Minimum H-inf norm: ' num2str(d_Cost) ' ===']);
else % The LMI terminated without finding a feasible solution
    display('No LMI solution exists!');
    return
end

%% Now build the controller
dM_Ke = [0 dM_K];
dM_AInt = ss_Plant.a+ss_Plant.b(:,2)*dM_Ke;
dM_BInt = ss_Plant.b(:,1);
ss_PlantInt = ss(dM_AInt,dM_BInt,[1 0 0 0],0);
tf_Prefilt = tf(d_z,[1 d_z]);
s=tf('s');
tf_Ko = d_g*(s+d_z)/s;
Try=(feedback(tf_Ko*ss_PlantInt,1)*tf_Prefilt);
Tre=(feedback(1,tf_Ko*ss_PlantInt));
%% Construct some maps and plot things
s_RetStr = '<html><head></head><body>';
figure(10);
h = step(Try,0:.01:d_SettlingTime*2);
plot(0:.01:d_SettlingTime*2,h);
if(isunix)
    print(10,'-djpeg','/home/pgaines/gocode/src/bitbucket.org/sriksrid/mosartomegaserver/matlab/users/ssridha7/img/Fig1.jpg');
    s_RetStr = [s_RetStr '<img src="/home/pgaines/gocode/src/bitbucket.org/sriksrid/mosartomegaserver/matlab/users/ssridha7/img/Fig1.jpg" ' ...
        'alt="The step response" />'];
end
bode(Tre);
if(isunix)
    print(gcf,'-djpeg','/home/pgaines/gocode/src/bitbucket.org/sriksrid/mosartomegaserver/matlab/users/ssridha7/img/Fig2.jpg');
    s_RetStr = [s_RetStr '<img src="/home/pgaines/gocode/src/bitbucket.org/sriksrid/mosartomegaserver/matlab/users/ssridha7/img/Fig2.jpg" ' ...
        'alt="The frequency response" />'];
end    
s_RetStr = [s_RetStr '</body></html>'];
